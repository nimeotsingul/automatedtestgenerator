from flask import Flask
from os import path

myapp = Flask(__name__, template_folder='../templates', static_folder='../static')
#print("teststt")
if path.exists('flask_config.py'):
    #print("Eksisteerib")
    from flask_config import AppConfig
    myapp.config.from_object(AppConfig)
    #print("Loaded")
    #print(myapp.config['SECRET_KEY'])


import app