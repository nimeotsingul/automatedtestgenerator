import sys
from estnltk import Text
from estnltk.taggers import GTMorphConverter
from estnltk.taggers import PronounTypeRetagger
import json
import shutil
import zipfile
import os
import csv
from tools.wordDifficultyChecker import WordDifficultyChecker

SEP = os.path.sep
replacedTagsAndForms = {}  # key: POStag või vorm, value: mitu korda on asendatud
# Antud argumendid
arguments = dict()


# Generaatori klass testide tegemiseks
class Generator:

    def __init__(self):
        self.wdc = WordDifficultyChecker()

    def createBlanks(self, text, partOfSpeech_list, lang_level=None, noun_form_list=None, verb_form_list=None,
                     pronoun_type_list=None,
                     age=None):
        # Loob EstNLTK 1.6 Text() objekti ja analüüsib teksti
        text = Text(text)
        stringTekst = text.text
        text.tag_layer(['words', 'sentences', 'morph_analysis', 'clauses'])

        # Erinevate layerite eraldamine
        words = text['words'].text
        tags = text['words'].partofspeech
        lemmas = text['words'].lemma

        GTconverter = GTMorphConverter()
        GTconverter.tag(text)
        forms = text['gt_morph_analysis'].form

        retagger = PronounTypeRetagger('morph_analysis')
        retagger.retag(text)
        pronounTypes = text.morph_analysis.pronoun_type

        wordsReplaced = 0
        offset = 0

        # Käib üle iga sõna tekstis
        for i in range(len(tags)):
            # Kontrollib kas sõna sõnaliik on see, mida sooviti.
            if tags[i][0] in partOfSpeech_list and \
                    ((tags[i][0] == "S" and (
                            noun_form_list is None or any(x in noun_form_list for x in forms[i][0].split(" ")))) or
                     (tags[i][0] == "V" and (
                             verb_form_list is None or any(y in verb_form_list for y in forms[i][0].split(" ")))) or
                     (tags[i][0] == "P" and (
                             pronoun_type_list is None or any(y in pronoun_type_list for y in pronounTypes[i][0]))) or
                     (tags[i][0] != "S" and tags[i][0] != "V" and tags[i][0] != "P")):

                # Kontrollib sõna raskusastet
                # wordFound on boolean, et kas sõna eksisteerib andmebaasis või mitte.
                # foundDifficulty on raskusaste juhul kui sõna leiti
                wordFound, foundDifficulty = self.wdc.findWordDifficulty(word=lemmas[i][0], wordPOSTag=tags[i][0],
                                                                         lang_level=lang_level,
                                                                         age=age)

                if not lang_level or wordFound:
                    # Salvestab sõna sõnaliigi esinemise kordi
                    if tags[i][0] in replacedTagsAndForms.keys():
                        replacedTagsAndForms[tags[i][0]] = replacedTagsAndForms[tags[i][0]] + 1
                    else:
                        replacedTagsAndForms[tags[i][0]] = 1

                    # Salvestab nimisõna ja tegusõna vorme
                    if tags[i][0] == "S" or tags[i][0] == "V":
                        # Vaatab sõna kõike vorme
                        for form in forms[i][0].split(" "):
                            saveForm = False
                            dict_form = ""
                            if tags[i][0] == "S" and (noun_form_list is None or form in noun_form_list):
                                if form == "Sg" or form == "Pl":
                                    dict_form = "S_" + form
                                else:
                                    dict_form = form
                                saveForm = True
                            elif tags[i][0] == "V" and (verb_form_list is None or form in verb_form_list):
                                if form == "Sg" or form == "Pl":
                                    dict_form = "V_" + form
                                else:
                                    dict_form = form
                                saveForm = True

                            if saveForm:
                                if dict_form in replacedTagsAndForms.keys():
                                    replacedTagsAndForms[dict_form] = replacedTagsAndForms[dict_form] + 1
                                else:
                                    replacedTagsAndForms[dict_form] = 1

                    # Asesõna puhul
                    if tags[i][0] == "P":
                        for pronoun_type in pronounTypes[i][0]:
                            saveForm = False
                            dict_type = ""
                            if pronoun_type_list is None or pronoun_type in pronoun_type_list:
                                dict_type = pronoun_type
                                saveForm = True

                            if saveForm:
                                if dict_type in replacedTagsAndForms.keys():
                                    replacedTagsAndForms[dict_type] = replacedTagsAndForms[dict_type] + 1
                                else:
                                    replacedTagsAndForms[dict_type] = 1

                    # Leiab tekstis sõna alguse ja lõpu ning asendab vajaliku koha tekstis ära.
                    wordStart = offset + text['words'][i].start
                    wordEnd = offset + text['words'][i].end
                    lemma = lemmas[i][0]
                    stringTekst = stringTekst[:wordStart] + "(" + lemma + ") " + "*" + words[i] + "*" + stringTekst[
                                                                                                        wordEnd:]
                    offset += 5 + len(lemma)  # 5 = "(" + ") " + "*" + "*"
                    wordsReplaced += 1
        print("Asendasin " + str(wordsReplaced) + " sõna.")
        return stringTekst

    # Loob H5P testifaili koos või ilma H5P.blanks moodulita
    def createH5Pfile(self, text, fileName, title, description=None, feedback=None, withModules=False):
        chosenTemplate = "smallest"
        if withModules:
            chosenTemplate = "minimumModules"
        # Kopeeri testi mall testide kausta
        try:
            shutil.copytree("h5p_template" + SEP + chosenTemplate, "h5ptests" + SEP + "h5p_template")
        except FileExistsError:
            # Eemaldab malli kausta
            shutil.rmtree("h5ptests" + SEP + "h5p_template")
            # Loob uue malli kausta
            shutil.copytree("h5p_template" + SEP + chosenTemplate, "h5ptests" + SEP + "h5p_template")

        # Lisab teksti, kirjelduse, tagasiside väärtused content.json faili
        file = open("h5ptests" + SEP + "h5p_template" + SEP + "content" + SEP + "content.json", "r", encoding='utf-8')
        data = file.read()
        d = json.loads(data)
        d["questions"] = ['<p>' + text + '</p>\n']
        if description:
            d["text"] = "<p>" + description + "</p>\n"
        if feedback:
            d['overallFeedback'] = feedback
        file.close()
        file = open("h5ptests" + SEP + "h5p_template" + SEP + "content" + SEP + "content.json", "w", encoding='utf-8')
        file.write(json.dumps(d))
        file.close()

        # Lisab pealkirja h5p.json faili
        file = open("h5ptests" + SEP + "h5p_template" + SEP + "h5p.json", "r", encoding='utf-8')
        data = file.read()
        d = json.loads(data)
        d["title"] = title
        file.close()
        file = open("h5ptests" + SEP + "h5p_template" + SEP + "h5p.json", "w", encoding='utf-8')
        file.write(json.dumps(d))
        file.close()

        # Loob H5P faili.
        zip = zipfile.ZipFile("h5ptests" + SEP + fileName, 'w')
        for folderName, subfolders, filenames in os.walk("h5ptests" + SEP + "h5p_template"):
            for filename in filenames:
                filePath = os.path.join(folderName, filename)
                zip.write(filePath, filePath.replace("h5ptests" + SEP + "h5p_template" + SEP, ""))
        zip.close()

        # Kustutab malli kausta
        shutil.rmtree("h5ptests" + SEP + "h5p_template")

    # Returns True, if inputs are correct. Returns False, if inputs are incorrect
    # Tagastab True, kui sisendid on õiged. Tagastab False, kui pole.
    # FILENAME;FILENAME POSTAG;POSTAG NOUN_FORM;NOUN_FORM VERB_FORM;VERB_FORM PRONOUN_TYPE;PRONOUN_TYPE AGE DIFFICULTY;DIFFICULTY MULTIPLE INDEKS TITLE MOODULID
    def check_inputs(self, inputs):
        POStag_list = ["A", "C", "D", "G", "H", "I", "J", "K", "N", "O", "P", "S", "U", "V", "X", "Y", "Z"]
        noun_form_list = ["Sg", "Pl", "Nom", "Gen", "Par", "Ill", "Ine", "Ela", "All", "Ade", "Abl", "Tra", "Trm",
                          "Ess", "Abe", "Com"]
        verb_form_list = ["Impers", "Pers", "Prs", "Prt", "Ind", "Cond", "Imprt", "Quot", "Sg", "Pl", "1", "2", "3",
                          "Aff",
                          "Neg", "Sup", "Inf", "Ger", "Prc"]
        pronoun_type_list = ["ps1", "ps2", "ps3", "rec", "refl", "pos", "dem", "inter_rel", "det", "indef"]
        difficulties_list = ["eelA1", "A1", "A2", "B1", "B2", "C1"]
        age_list = ["noor", "taiskasvanu"]

        if len(inputs) != 12:
            print("Vale number parameetreid.")
            return False

        files = inputs[1]
        POStags = inputs[2]
        noun_forms = inputs[3]
        verb_forms = inputs[4]
        pronoun_types = inputs[5]
        age = inputs[6]
        difficulties = inputs[7]
        multiple = inputs[8]
        indeksid = inputs[9]
        title = inputs[10]
        modules = inputs[11]

        if not all(os.path.isfile("inputs" + SEP + file) for file in files.split(";")):
            print("Viga. Tekstifaili ei leitud.")
            return False
        elif any(POStag not in POStag_list for POStag in POStags.split(";")):
            print("Viga. Etteantud sõnaliik pole sobiv.")
            return False
        elif noun_forms != "None" and "S" in POStags.split(";") and any(
                noun_form not in noun_form_list for noun_form in noun_forms.split(";")):
            print("Viga. Etteantud nimisõnavorm pole sobiv.")
            return False
        elif verb_forms != "None" and "V" in POStags.split(";") and any(
                verb_form not in verb_form_list for verb_form in verb_forms.split(";")):
            print("Viga. Etteantud tegusõnavorm pole sobiv.")
            return False
        elif pronoun_types != "None" and "P" in POStags.split(";") and any(
                pronoun_type not in pronoun_type_list for pronoun_type in pronoun_types.split(";")):
            print("Viga. Etteantud asesõna tüüp pole sobiv.")
            return False
        elif age != "None" and age not in age_list:
            print("Viga. Vanuse väärtus peab olema noor või taiskasvanu.")
            return False
        elif difficulties != "None" and any(diff not in difficulties_list for diff in difficulties.split(";")):
            print("Viga. Etteantud raskusaste pole sobiv.")
            return False
        elif multiple not in ['True', 'False']:
            print("Viga. Mitme testi loomise argument peab olema True või False.")
        elif not indeksid == 'None' and any(not indeks.isnumeric() and (
                not len(indeks.split('-')) == 2 or any(not pool.isnumeric() for pool in indeks.split('-'))) for indeks
                                            in indeksid.split(';')):
            print('Indeksite sisend on vale.')
            return False
        elif modules not in ['True', 'False']:
            print(
                "Viga. H5P formaati polnud täpsustatud. True - H5P_blanks mooduliga. False - miinimum (h5p.json ja content.json).")
        else:
            if age == "None" and difficulties != "None":
                difficulties = "None"
            if modules == "True":
                modules = True
            else:
                modules = False
            arguments['files'] = files
            arguments['POStags'] = POStags
            arguments['noun_forms'] = noun_forms
            arguments['verb_forms'] = verb_forms
            arguments['pronoun_types'] = pronoun_types
            arguments['age'] = age
            arguments['difficulties'] = difficulties
            arguments['multiple'] = multiple
            arguments['indeksid'] = indeksid
            arguments['title'] = title
            arguments['modules'] = modules
            return True

    def make_tests_and_csv(self, argumentList):
        if self.check_inputs(argumentList):
            columnNames = ["Fail", "H5P_fail",
                           "A", "C", "D", "G", "H", "I", "J", "K", "N", "O", "P", "S", "U", "V", "X", "Y", "Z",
                           "S_Sg", "S_Pl", "Nom", "Gen", "Par", "Ill", "Ine", "Ela", "All", "Ade", "Abl", "Tra", "Trm",
                           "Ess", "Abe", "Com",
                           "Impers", "Pers", "Prs", "Prt", "Ind", "Cond", "Imprt", "Quot", "V_Sg", "V_Pl", "1", "2",
                           "3", "Aff", "Neg",
                           "Sup", "Inf", "Ger", "Prc",
                           "ps1", "ps2", "ps3", "rec", "refl", "pos", "dem", "inter_rel", "det", "indef",
                           "noor", "taiskasvanu",
                           "eelA1", "A1", "A2", "B1", "B2", "C1", "Pealkiri"]

            # Kui CSV faili pole, siis tee uus ja lisa veerud.
            if not os.path.isfile("h5ptests" + SEP + "tests.csv"):
                csvFile = open("h5ptests" + SEP + "tests.csv", mode='w', newline='')
                csvWriter = csv.writer(csvFile)
                csvWriter.writerow(columnNames)

            csvFile = open("h5ptests" + SEP + "tests.csv", mode='a', newline='')
            csvWriter = csv.writer(csvFile)

            # Eralda argumendid
            fileNames = arguments.get('files').split(";")
            POStags = arguments.get('POStags').split(";")
            noun_form_list = arguments.get('noun_forms').split(";")
            if "None" in noun_form_list:
                noun_form_list = None
            verb_form_list = arguments.get('verb_forms').split(";")
            if "None" in verb_form_list:
                verb_form_list = None
            pronoun_type_list = arguments.get('pronoun_types').split(";")
            if "None" in pronoun_type_list:
                pronoun_type_list = None
            lang_level = arguments.get('difficulties').split(";")
            if "None" in lang_level:
                lang_level = None
            age = arguments.get('age')
            if age == "None":
                age = None
            multipleTests = arguments.get('multiple')
            if arguments.get('indeksid') == 'None':
                indeksid = None
            else:
                indeksid = set()
                for indeks in arguments.get('indeksid').split(';'):
                    if indeks.isnumeric():
                        indeksid.add(int(indeks))
                    else:
                        pooled = indeks.split('-')
                        if pooled[0] < pooled[1]:
                            for i in range(int(pooled[0]), int(pooled[1]) + 1):
                                indeksid.add(i)
                        elif pooled[0] == pooled[1]:
                            indeksid.add(int(pooled[0]))
                        else:
                            for i in range(int(pooled[1]), int(pooled[0]) + 1):
                                indeksid.add(i)

            # Loendab teste indeksite järgi
            testiIndeks = 0

            for i in range(len(fileNames)):
                # Loeb iga etteantud tekstifaili
                file = open("inputs" + SEP + fileNames[i], "r", encoding='utf-8', errors='ignore')
                inputText = file.read()
                file.close()

                # Kontrollib kas igast failist soovitakse teha mitu testi.
                texts = []
                if multipleTests == "True":
                    for text in inputText.split('\n\n<ERALDA>\n\n'):
                        texts.append(text)
                else:
                    inputText = inputText.replace('\n\n<ERALDA>\n\n', '\n\n')
                    texts.append(inputText)

                # Loo lünkadega tekst ja tee nendest H5P failid
                for j in range(len(texts)):
                    global replacedTagsAndForms
                    replacedTagsAndForms = {}

                    temp_text = Text(texts[j])
                    temp_text.tag_layer(['words'])
                    words = temp_text['words'].text
                    if len(words) < 6:
                        if not indeksid:
                            print('''Tekst "''' + texts[j] + '''" jäeti pikkuse tõttu tekstiks tegemata''')
                        continue

                    # Kui tehtakse indeksite järgi, vaatab kas hetkene test on otsitav
                    if indeksid and testiIndeks not in indeksid:
                        testiIndeks = testiIndeks + 1
                        continue

                    textWithBlanks = self.createBlanks(text=texts[j], partOfSpeech_list=POStags, lang_level=lang_level,
                                                       noun_form_list=noun_form_list, verb_form_list=verb_form_list,
                                                       pronoun_type_list=pronoun_type_list,
                                                       age=age)

                    # Muul juhul suurendab indeksit ja jätkab
                    testiIndeks = testiIndeks + 1
                    testFileName = fileNames[i] + "." + str(j + 1) + ".h5p"
                    title = arguments.get('title')
                    self.createH5Pfile(textWithBlanks, testFileName, title, None, None, arguments.get('modules'))
                    print(testFileName + " edukalt loodud!")

                    nextCSVline = [0] * len(columnNames)
                    nextCSVline[columnNames.index("Fail")] = fileNames[i]
                    nextCSVline[columnNames.index("H5P_fail")] = testFileName
                    nextCSVline[columnNames.index("Pealkiri")] = title

                    for key in replacedTagsAndForms.keys():  # Tags and forms
                        nextCSVline[columnNames.index(key)] = replacedTagsAndForms[key]
                    if age:
                        nextCSVline[columnNames.index(age)] = 1
                        if lang_level:
                            for level in lang_level:
                                nextCSVline[columnNames.index(level)] = 1
                    csvWriter.writerow(nextCSVline)

            csvFile.close()
            print("Loodud testi(d) leiab: " + os.getcwd() + SEP + "h5ptests" + SEP)


if __name__ == "__main__":
    Generator().make_tests_and_csv(sys.argv)
