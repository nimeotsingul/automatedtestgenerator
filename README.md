[Project description](https://gitlab.com/nimeotsingul/automatedtestgenerator/-/wikis/uploads/6eb23eb65a633102ed2fa4d21a0a945c/HAK-AutomatedtestgeneratorforEstonianlanguage-280820-1604-41999.pdf)

# Project members
- [Taaniel Saarnik](https://gitlab.com/TaanielS)
- [Maert Mägi](https://gitlab.com/Nsx)
- [Andrus Müür](https://gitlab.com/andrusmuur)
- [Martin Nõmm](https://gitlab.com/martinnomm)

# Iterations
- [1st iteration](https://gitlab.com/nimeotsingul/automatedtestgenerator/-/wikis/1st-iteration)
- [2nd iteration](https://gitlab.com/nimeotsingul/automatedtestgenerator/-/wikis/2nd-iteration)
- [3rd iteration](https://gitlab.com/nimeotsingul/automatedtestgenerator/-/wikis/3rd-iteration)
- [4th iteration](https://gitlab.com/nimeotsingul/automatedtestgenerator/-/wikis/4th-iteration)


Anaconda with Python 3.7 was used as an project interpreter.  
Estonian guide: [Anaconda keskkonna seadistamine](https://gitlab.com/nimeotsingul/automatedtestgenerator/-/wikis/Anaconda-environment-setup-(EST))  
English guide: [Anaconda environment setup](https://gitlab.com/nimeotsingul/automatedtestgenerator/-/wikis/Anaconda-environment-setup-(ENG))  


In order to make multiple tests from one text file, we decided on the following divider (empty_line &lt;ERALDA&gt; empty_line):  
```
> Some text for a test.
> More some text.
> 
> <ERALDA>
> 
> Another test's text.
```

You can also look at [input.example](https://gitlab.com/nimeotsingul/automatedtestgenerator/-/blob/master/input.example) and [input.example2](https://gitlab.com/nimeotsingul/automatedtestgenerator/-/blob/master/input.example2) files for better understanding.

