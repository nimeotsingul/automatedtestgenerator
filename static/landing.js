$(document).ready(function () {
    // NAVBAR - source: https://codepen.io/AleksandrHovhannisyan/pen/WNQzawO
    const navbar = $('#navbar');
    const navbarToggle = $('.navbar-toggle');

    function openMobileNavbar() {
        navbar.addClass('opened')
        navbarToggle.attr("aria-label", "Close navigation menu.");
    }

    function closeMobileNavbar() {
        navbar.removeClass('opened')
        navbarToggle.attr("aria-label", "Open navigation menu.");
    }

    navbarToggle.click(() => {
        if (navbar.hasClass('opened'))
            closeMobileNavbar();
        else
            openMobileNavbar();
    });

    const navbarMenu = $('.navbar-menu')
    const navbarLinksContainer = $('.navbar-links');

    navbarLinksContainer.click((clickEvent) => {
        clickEvent.stopPropagation();
    });

    navbarMenu.on('click', closeMobileNavbar);

    // RADIOBUTTON
    const young = $('.level-selection-young');
    const adult = $('.level-selection-adult');
    const radio = $('input[type=radio][name=age]');

    radio.change(function () {
        if (this.value === 'young' && adult.hasClass('active')) {
            adult.removeClass('active');
            young.addClass('active');
            adult.find('input[type=checkbox]').prop('checked', false);
        } else if (this.value === 'adult' && young.hasClass('active')) {
            young.removeClass('active');
            adult.addClass('active');
            young.find('input[type=checkbox]').prop('checked', false);
        }
    });

    // MULTISELECT
    $('select[multiple]').multiselect();
    $('#pos-type_input').attr('placeholder', 'Vali sõnaliik')
    $('#noun_input').attr('placeholder', 'Vali nimisõnavorm')
    $('#verb_input').attr('placeholder', 'Vali tegusõnavorm')
    $('#pronoun_input').attr('placeholder', 'Vali asesõnavorm')


    // Default feedback row
    let defaultRow =
        '<tr class="feedback-row">' +
        '    <td class="range-from">0 %</td>' +
        '    <td class="dash">-</td>' +
        '    <td class="range-to">100 %</td>' +
        '    <td class="range-label">' +
        '        <input class="range-label-input" name="range-label" type="text" placeholder="Täida tagasiside">' +
        '    </td>\n' +
        '    <td class="remove-range">' +
        '        <button type="button" class="remove-range-btn">' +
        '            <span class="icon-bar"></span>' +
        '            <span class="icon-bar"></span>' +
        '        </button>' +
        '    </td>' +
        '</tr>'

    if ($('.feedback-row').length === 0) {
        $('.feedback-table tbody').append(defaultRow);
    }

    // Adding feedback rows
    $(document).on('click', '#add-range-btn', function () {
        let feedbackRow = $('.feedback-row')
        let feedbackTable = $('.feedback-table tbody')
        if (feedbackRow.length === 0) {
            feedbackTable.append(defaultRow);
        } else {
            feedbackRow.last().find('.range-to').html('<input class="range-num-input" name="range" type="number" min="0" max="100">')
            let newRow =
                '<tr class="feedback-row">' +
                '   <td class="range-from"></td>' +
                '   <td class="dash">-</td>' +
                '   <td class="range-to">100 %</td>' +
                '   <td class="range-label"><input class="range-label-input" name="range-label" type="text" placeholder="Täida tagasiside"></td>' +
                '   <td class="remove-range">' +
                '       <button type="button" class="remove-range-btn">' +
                '           <span class="icon-bar"></span>' +
                '           <span class="icon-bar"></span>' +
                '       </button>' +
                '   </td>' +
                '</tr>';

            feedbackTable.append(newRow);
        }
    });

    // Feedback input listeners
    $(document).on('focusout', '.range-num-input', function () {

        let self = $(this)
        let currentRow = self.closest('.feedback-row')
        let nextRow = currentRow.next()

        let val = parseInt(self.val())
        let min = parseInt(currentRow.prev().find('.range-num-input').val())

        let error = '<div class="error">ERROR</div>'
        let errorBorder = '2px solid #da0001'
        let normalBorder = '1px solid rgba(51, 51, 51, 0.5)'

        currentRow.removeClass('error-container')
        currentRow.find('.error').remove()
        // Monstrosity
        if (!isNaN(val)) {
            if (val > 100 || val < 0 || val <= min) {
                self.css('border', errorBorder)
                currentRow.addClass('error-container')
                if (val > 100) error = error.replace('ERROR', 'Numbrivälja väärtus peab olema 100 piires.')
                if (val < 0) error = error.replace('ERROR', 'Numbrivälja väärtus peab olema vähemalt 0.')
                if (val <= min) error = error.replace('ERROR', 'Numbrivälja väärtus peab olema suurem kui ' + min + '.')
                currentRow.find('.range-to').append(error)
                nextRow.find('.range-from').empty();
            } else {
                self.css('border', normalBorder)
                $('#output-form-error').html('')
                if (val === 100) {
                    nextRow.find('.range-from').html(val + ' %')
                } else {
                    nextRow.find('.range-from').html((val + 1) + ' %')
                }
            }
        } else {
            self.css('border', errorBorder)
            currentRow.addClass('error-container')
            error = error.replace('ERROR', 'Numbriväli tohib sisaldada ainult numbreid.')
            currentRow.find('.range-to').append(error)
            nextRow.find('.range-from').empty();
        }
    });


    // REMOVE FEEDBACK ROW
    $(document).on('click', '.remove-range-btn', function () {
        let self = $(this)
        let currentRow = self.closest('.feedback-row')
        let previousRow = currentRow.prev()
        let nextRow = currentRow.next()

        if (currentRow.is(':first-child')) {
            nextRow.find('.range-from').html('0 %')
        } else if (currentRow.is(':last-child')) {
            previousRow.find('.range-to').html('100 %')
        } else {
            nextRow.find('.range-from').html(currentRow.find('.range-from').text())
        }
        self.parents('tr').remove();
    });

    // COLOURED TEXT
    let asterisks = /\*(.*?)\*/g;
    let marked = '<span class="marked">*$1*</span>';

    let h5pFormat = /\((.*?)\) \*(.*?)\*/g;
    let h5pMarked = '<span class="marked">($1) *$2*</span>';

    function markText(text) {
        return text.replace(h5pFormat, h5pMarked);
    }

    // Text highlighting - source: https://gist.github.com/zserge/03280e5efebae83cc41c78dbd7e73608
    const js = el => {
        for (const node of el.children) {
            const s = node.innerText
                .replace(h5pFormat, h5pMarked);
            node.innerHTML = s.split('\n').join('<br>');
        }
    };

    const editor = (el, highlight = js) => {
        const caret = () => {
            const range = window.getSelection().getRangeAt(0);
            const prefix = range.cloneRange();
            prefix.selectNodeContents(el);
            prefix.setEnd(range.endContainer, range.endOffset);
            return prefix.toString().length;
        };

        const setCaret = (pos, parent = el) => {
            for (const node of parent.childNodes) {
                if (node.nodeType == Node.TEXT_NODE) {
                    if (node.length >= pos) {
                        const range = document.createRange();
                        const sel = window.getSelection();
                        range.setStart(node, pos);
                        range.collapse(true);
                        sel.removeAllRanges();
                        sel.addRange(range);
                        return -1;
                    } else {
                        pos = pos - node.length;
                    }
                } else {
                    pos = setCaret(pos, node);
                    if (pos < 0) {
                        return pos;
                    }
                }
            }
            return pos;
        };

        highlight(el);

        el.addEventListener('keyup', e => {
            if (e.keyCode >= 0x30 || e.keyCode == 0x20) {
                const pos = caret();
                highlight(el);
                setCaret(pos);
                if (el.length !== 0)
                    $('#output-form-error').html('');
            }
        });
    };

    // Turn div into an editor
    const el = document.querySelector('.editor');
    editor(el);

    // Reset form error
    $('.l-textbox .textbox, #test-title, #test-description, .multiselect-list').on('input change', function () {
        $('#input-form-error').html('')
    });

    // POST
    async function postGeneratorInput() {
        let inText = $('.l-textbox .textbox').text();
        let inTitle = $('#test-title').val();
        let inDesc = $('#test-description').val();
        let inAge = $('input[name="age"]:checked').val();
        let inLangLevel = $.map($('input[name="level"]:checked'), function (c) {
            return c.value;
        });
        let inPostypes = $('select[name="pos-type"]').val();
        let inNounforms = $('select[name="noun"]').val();
        let inVerbforms = $('select[name="verb"]').val();
        let inPronounForms = $('select[name="pronoun"]').val();

        let formError = $('#input-form-error');

        let result;

        if (inText === '') {
            formError.html('Sisendi tekst puudub.')
        } else if (inTitle === '') {
            formError.html('Pealkiri puudub.')
        } else if (inPostypes.length === 0) {
            formError.html('Vali sõnaliik.')
        } else {
            try {
                let formData = {
                    text: inText,
                    title: inTitle,
                    description: inDesc,
                    age: inAge,
                    langlevel: inLangLevel,
                    postypes: inPostypes,
                    nounforms: inNounforms,
                    verbforms: inVerbforms,
                    pronounforms: inPronounForms,
                };
                result = await $.ajax({
                    type: 'POST',
                    data: formData,
                    url: '/Create',
                    dataType: "json",
                    success: function(response) {
                        $('#h_text').val(response.text);
                        $('#h_title').val(response.title);
                        $('#h_desc').val(response.description);
                        fillOutput(response.text);
                    }
                });
                return result;
            } catch (error) {
                console.log(error);
            }
        }
        return false;
    }

    // GET
    async function getGeneratorOutput() {
        let result;
        try {
            result = await $.ajax({
                type: 'GET',
                url: '/Create',
            });
            return result;
        } catch (error) {
            console.log(error);
        }
    }

    function fillOutput(text) {
        let editor = $('.editor div');
        editor.html(text);
        editor.html(markText(editor.text()))
    }

    $('#create-btn').click(async function (e) {
        e.preventDefault();
        //Loading spinner
        let spinner = $('.lds-container')
        spinner.css('display', 'flex')

        await postGeneratorInput().then(function () {
            spinner.css('display', 'none')
        });
    });

    function getFeedback() {
        let fromArr = $('.range-from').map(function () {
            return parseInt($(this).text().split(' ')[0]);
        }).get();
        let toArr = $('.range-num-input').map(function () {
            return parseInt($(this).val());
        }).get();
        toArr.push(100);
        let msgArr = $('.range-label-input').map(function() {
            return $(this).val().toString();
        }).get();

        return fromArr.map(function (v, i) {
            return {
                from: v,
                to: toArr[i],
                message: msgArr[i]
            };
        });
    }

    async function h5pInput() {
        let outText = $('.editor').text();
        let inTitle = $('#test-title').val();
        let inDesc = $('#test-description').val();
        let result;

        let formError = $('#output-form-error');

        let feedback = getFeedback();
        for (const el of feedback) {
            if (isNaN(el.from) || isNaN(el.to)) {
                feedback = "ERROR";
            }
        }
        if (outText.trim() !== '') {
            if ($('.feedback-table').find('.error').length === 0 && feedback !== "ERROR") {
                try {
                    result = await $.ajax({
                        type: 'POST',
                        data: {
                            text: outText,
                            overallFeedback: feedback
                        },
                        url: '/Download',
                        success: function(response) {
                            result = response
                            $('#h_text').val(response.text);
                            $('#h_scoring').val(response.feedback);
                            $('#h_title').val(inTitle);
                            $('#h_desc').val(inDesc);
                        }
                    });
                    document.hidden_form.submit()
                    return result;
                } catch (e) {
                    console.log(e);
                }
            } else {
                formError.html('Hindamisskaala sisaldab vigu.')
            }
        } else {
            formError.html('Väljundi tekst puudub.')
        }
    }

    $('#publish-btn').click(async function (e) {
        e.preventDefault();
        await h5pInput();
    });
});
