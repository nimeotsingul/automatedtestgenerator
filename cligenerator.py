from cliapp.generator import Generator
from tools.analyzer import Analyzer
import sys
import time

# python cligenerator.py TEKSTIFAIL;TEKSTIFAIL SÕNALIIK;SÕNALIIK NIMISÕNA_VORM;NIMISÕNA_VORM TEGUSÕNA_VORM;TEGUSÕNA_VORM ASESÕNA;ASESÕNA VANUS RASKUSASTE;RASKUSASTE MITUTESTI INDEKSID PEALKIRI MOODULID
if sys.argv[1] == "--info":
    print("----------------------------------------------------------------------------------------------------------------------------------------------")
    print('GENERAATOR:')
    print('CLI SISEND: python cligenerator.py TEKSTIFAIL;TEKSTIFAIL SÕNALIIK;SÕNALIIK NIMISÕNA_VORM;NIMISÕNA_VORM TEGUSÕNA_VORM;TEGUSÕNA_VORM ASESÕNA_VORM;ASESÕNA_VORM VANUS RASKUSASTE;RASKUSASTE MITUTESTI INDEKSID PEALKIRI MOODULID')
    print('-tekstifail')
    print('\t Tekstifail või ";" semikooloniga eraldatud mitu tekstifail;tekstifail;tekstifail')
    print('\t Tekstifailid peavad olema "inputs" kaustas')
    print('-sõnaliik')
    print('\t Sõnaliik või ";" semikooloniga eraldatud mitu sõnaliik;sõnaliik;sõnaliik')
    print('\t Ei saa olla "None"')
    print('-nimisõna_vorm')
    print('\t Nimisõna_vorm või ";" semikooloniga eraldatud mitu nimisõna_vorm;nimisõna_vorm;nimisõna_vorm')
    print('\t Nimisõna_vorm saab olla "None", siis otsitakse kõiki nimisõna vorme')
    print('-tegusõna_vorm')
    print('\t Tegusõna_vorm või ";" semikooloniga eraldatud mitu tegusõna_vorm;tegusõna_vorm;tegusõna_vorm')
    print('\t Tegusõna_vorm saab olla "None", siis otsitakse kõiki tegusõna vorme')
    print('-asesõna')
    print('\t Asesõna või ";" semikooloniga eraldatud mitu asesõna;asesõna;asesõna')
    print('\t Asesõna saab olla "None", siis otsitakse kõiki asesõna vorme')
    print('-vanus')
    print('\t Vanuse järgi vaadatakse sõnade sõnaveebi määratud raskusastet')
    print('\t Vanus saab olla kas noor või täiskasvanu')
    print('\t Vanus saab olla ka "None", sel juhul ei kontrollita üldse vanust ja raskusastet')
    print('-raskusaste')
    print('\t Raskusaste või ";" semikooloniga eraldatud mitu raskusaste;raskusaste;raskusaste')
    print('\t Raskusaste saab olla "None", siis lastakse kõik raskusastmed läbi')
    print('\t Raskusaste on määratud sõnaveebi andmebaasi raskuste järgi')
    print('-mitutesti')
    print('\t Mitutesti sisend saab olla kas "True" või "False"')
    print('\t Mitutesti määrab, kas failist tehtakse mitu testi või kõik kokku')
    print('\t Failis eraldatakse testid <ERALDA> vahele panemise kaudu')
    print('-indeksid')
    print('\t Indeksid sisend on üksik või eraldatud ";" semikooloniga või "-" sidekriipsuga')
    print('\t Indeksid sisendi näide: 2;3-6 loob testid 2 ja 3 kuni 6')
    print('\t Indeksid saab olla ka "None", sel juhul tehtakse valmis kõik testid tekstifailidest')
    print('\t Indeksid on analyze kaudu saadud indeksid')
    print('\t Indeksid NB!! kui kasutate mitut tekstifaili analüüsijas, siis tuleb samade indeksite jaoks kasutada samu tekstifaile ka generaatoris')
    print('-pealkiri')
    print('\t Pealkiri, mis läheb testidele')
    print('-moodulid')
    print('\t Moodulid sisend on "True" või "False"')
    print('\t Moodulid määrab, kas valmistatud testid on muude H5P lisanditega või mitte')
    print("----------------------------------------------------------------------------------------------------------------------------------------------")
    print('ANALÜÜSIJA:')
    print('CLI SISEND: python cligenerator.py --analyze TEKSTIFAIL;TEKSTIFAIL MITUTESTI')
    print('-tekstifail')
    print('\t Tekstifail või ";" semikooloniga eraldatud mitu tekstifail;tekstifail;tekstifail')
    print('\t Tekstifailid peavad olema "inputs" kaustas')
    print('-mitutesti')
    print('\t Mitutesti sisend saab olla kas "True" või "False"')
    print('\t Mitutesti määrab, kas failist tehtakse mitu testi või kõik kokku')
    print('\t Failis eraldatakse testid <ERALDA> vahele panemise kaudu')
elif sys.argv[1] == '--analyze':
    Analyzer().analyzeCSV(sys.argv)
else:
    Generator().make_tests_and_csv(sys.argv)
