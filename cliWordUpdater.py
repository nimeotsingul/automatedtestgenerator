from tools.utils import resourceEKITranslator
from resources import resourcesFile
from tools import wordDifficultyChecker
import sys
import json
import os
import time
import inspect

# Fail, millega saab uuendada sõnaveebi faile
# Uus fail tuleb panna resources kausta
# Faili saab alla laadida sonaveebist
# URL https://sonaveeb.ee/teacher-tools/
# Sealt Sõnavara otsing, valid vanuse (filtrid kõik maha) ja tõmbad txt faili
# txt fail asetada resources kausta
# Kasutusjuhend on --info käsuga


def displayInfo():
    # Ainult --info
    if len(sys.argv) == 2:
        print("----------------------------------------------------------------------------------------------------------------------------------------------")
        print("Käsud: Kasuta käsku peale --info, et saada abi selle käsuga (näiteks: --info --change")
        print("--info \t\t prindib välja selle sama teksti")
        print("--current \t prindib välja hetkel kasutusel sõnaveebi failid ja millal neid viimati muudeti")
        print("--change \t muudab hetkel kasutusel tõlgitud andmebaase NB! fail peab olema  _translated.json fail resources kaustas")
        print("--new \t\t tõlgib uue sõnaveebi faili ja paneb ta kasutusele andmebaaside jaoks testidele")
        print("----------------------------------------------------------------------------------------------------------------------------------------------")
    elif len(sys.argv) == 3: # --info käsk
        print("----------------------------------------------------------------------------------------------------------------------------------------------")
        if sys.argv[2] == "--info":
            print("Prindib info, kasutades koos muu käsuga annab käsu kohta infot(here you are)")
        elif sys.argv[2] == "--current":
            print("--current ei võta rohkem argumente")
            print("Prindib välja milliseid andmebaasi nimesid kasutatakse hetkel noore ja täiskasvanu jaoks ning millal viimati neid muudeti")
        elif sys.argv[2] == "--change":
            print("--change võtab 2 argumenti, tõlgitud_failinimi ja vanus")
            print("Näiteks: --change EKI_noor_translated.json noor")
            print("Muudab kasutusel olevaid sõnaveebi andmebaase kindla vanuse suhtes testide jaoks")
            print("Faili nimi peab olema _translated.json resources kaustas")
            print("Vanus peab olema noor või täiskasvanu")
        elif sys.argv[2] == "--new":
            print("--new võtab 2 argumenti, failinimi ja vanus")
            print("Näiteks: --new EKI_noor.txt noor")
            print("Tõlgib uue sõnaveebi txt andmebaasi faili ja paneb selle hetkel kasutusel olevate alla")
            print("Failinimi peab lõppema .txt ja asuma resources kaustas, saab alla laadida Sõnaveebi andmebaasist")
            print("Vanus peab olema noor või täiskasvanu")
        else:
            print("Ei saanud aru käsust: {}".format(sys.argv[2]))
        print("----------------------------------------------------------------------------------------------------------------------------------------------")
    else:
        print("Vale arv parameetreid")


# Näitab hetkel kasutusel olevaid JSON faile ja nende muutmis aega
def displayCurrent():
    jsonData = readJSONFile()

    youngDate = time.strftime('%d/%m/%Y', time.gmtime(os.path.getmtime(os.path.join(os.path.dirname(inspect.getfile(resourcesFile)), jsonData.get("young_translated")))))
    adultDate = time.strftime('%d/%m/%Y', time.gmtime(os.path.getmtime(os.path.join(os.path.dirname(inspect.getfile(resourcesFile)), jsonData.get("adult_translated")))))

    print("----------------------------------------------------------------------------------------------------------------------------------------------")
    print("Hetkel kasutusel olevad tõlgitud json failid ja nende muutmise aeg (Pole kindel kas seoses selle ajaga, millal need alla laeti, vaid loodi)")
    print("Noorte tõlgitud fail:", jsonData.get("young_translated"), ", Last changed:", youngDate)
    print("Täiskasvanute tõlgitud fail:", jsonData.get("adult_translated"), ", Last changed:", adultDate)
    print("----------------------------------------------------------------------------------------------------------------------------------------------")


# Muudab json faili vanemaks (Juhul kui on vaja tagasi muuta)
def changeCurrent():
    # Kontrollib argumentide hulka (failinimi, vanus)
    if len(sys.argv) == 4:
        translatedFileName = sys.argv[2]
        age = sys.argv[3]
        if checkFileAndAge(translatedFileName, age):
            if translatedFileName.endswith("_translated.json"):
                if age == 'noor':
                    age = 'young'
                if age == 'täiskasvanu':
                    age = 'adult'

                # Muudab JSON data
                jsonData = readJSONFile()
                jsonData[age + "_translated"] = translatedFileName
                saveJSONFile(jsonData)
                print("Vanuse {} jaoks muudeti fail selleks: {}".format(age, translatedFileName))
            else:
                print("Faili ei saanud tõlkida json failiks")
    else:
        print("Vale arv parameetreid")


# Loob ja tõlgib uued andmebaasi failid ning paneb nad kasutusele testide jaoks
def changeNew():
    # Kontrollib argumentide hulka (failinimi, vanus)
    if len(sys.argv) == 4:
        fileName = sys.argv[2]
        age = sys.argv[3]
        if checkFileAndAge(fileName, age):
            if fileName.endswith(".txt"):

                print("Tõlgib {} vanuse {} jaoks, võib minna mõni minut".format(fileName, age))
                translatedFileName = resourceEKITranslator.translateFile(fileName)

                if age == 'noor':
                    age = 'young'
                if age == 'täiskasvanu':
                    age = 'adult'

                # Change JSON data
                jsonData = readJSONFile()
                jsonData[age] = fileName
                jsonData[age + "_translated"] = translatedFileName
                saveJSONFile(jsonData)

                print("Tõlkimine on lõpetatud, vanus {} kasutab nüüd {}".format(age, translatedFileName))
            else:
                print("Fail pole .txt lõpuga")
    else:
        print("Vale arv parameetreid")


# Kontrollib kas fail on olemas ja kas vanus õige
def checkFileAndAge(fileName, age):
    # Kontrollib kas vanus õige (noor/täiskasvanu)
    if age == "noor" or age == "täiskasvanu":
        # Kontrollib kas fail olemas
        filePath = os.path.join(os.path.dirname(inspect.getfile(resourcesFile)), fileName)
        if os.path.exists(filePath):
            return True
        else:
            print("Ei leidnud faili {} resources kaustast".format(fileName))
    else:
        print("Vale vanuse sisend")
        return False


def readJSONFile():
    # Loeb sisse JSON faili
    jsonPath = os.path.join(os.path.dirname(inspect.getfile(wordDifficultyChecker)),
                            "wordDifficulty_config.json")

    with open(jsonPath, encoding='utf8') as json_file:
        jsonData = json.load(json_file)

    return jsonData


def saveJSONFile(jsonData):
    # Dumpib andmed JSON faili
    jsonPath = os.path.join(os.path.dirname(inspect.getfile(wordDifficultyChecker)),
                            "wordDifficulty_config.json")

    with open(jsonPath, 'w', encoding='utf8') as outfile:
        json.dump(jsonData, outfile, ensure_ascii=False)


def cliWordUpdater():
    if len(sys.argv) == 1:
        print("Kasutage parameetrit --info abi jaoks")
    elif len(sys.argv) >= 2:
        if sys.argv[1] == "--info":
            displayInfo()
        elif sys.argv[1] == "--current":
            displayCurrent()
        elif sys.argv[1] == "--change":
            changeCurrent()
        elif sys.argv[1] == "--new":
            changeNew()
        else:
            print("Ei saanud aru käsust, {}".format(sys.argv[1]))
    else:
        print("Tundmatu parameeter, kasutage --info")


if __name__ == "__main__":
    cliWordUpdater()
