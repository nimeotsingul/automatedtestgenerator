import unittest
import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from cliapp.generator import Generator
import json
import zipfile
import shutil

SEP = os.path.sep


class MyTestCase(unittest.TestCase):
    def setUp(self):
        if "automatedtestgenerator" in os.listdir():
            os.chdir('automatedtestgenerator')
        if 'test_tagger.py' in os.listdir():
            os.chdir('..')

    def test_blanks(self):
        rista_tekst = '''Ristal alustab sellest, kuidas ta Wolti juhi kohale üldse jõudis. 400 teist kandidaati, kaheksa pikka vestlust, kolm keerulist kodutööd, 120-slaidine tutvustus selle kohta, kuidas ta firma üles ehitaks. «Kui ma olin kogu selle kadalipu läbinud, ütlesin omanikele, et lõpetame selle jama ära, andke mulle leping, ma ju juba töötan teie jaoks,» meenutab Ristal naerdes. Tema motivatsioon oli kõrge, sest tal oli kindel plaan, missugune peaks moodne toidu kojuveo firma olema.'''
        text = "Ta läks ära ja tuli hiljem tagasi. Mari polnud kindel, mida ta tegema peaks ja jättis hüvasti."
        text_w_blanks = Generator().createBlanks(text=text, partOfSpeech_list=["V"])
        self.assertEqual(
            "Ta (minema) *läks* ära ja (tulema) *tuli* hiljem tagasi. Mari (olema) *polnud* kindel, mida ta"
            " (tegema) *tegema* (pidama) *peaks* ja (jätma) *jättis* hüvasti.", text_w_blanks)

        text_w_blanks = Generator().createBlanks(text=text, partOfSpeech_list=["V"], verb_form_list=["Sg"])
        self.assertEqual("Ta (minema) *läks* ära ja (tulema) *tuli* hiljem tagasi. Mari polnud kindel, mida ta"
                         " tegema peaks ja (jätma) *jättis* hüvasti.", text_w_blanks)

        text_w_blanks = Generator().createBlanks(text=text, partOfSpeech_list=["P"])
        self.assertEqual("(tema) *Ta* läks ära ja tuli hiljem tagasi. Mari polnud "
                         "kindel, (mis) *mida* (tema) *ta* tegema peaks ja jättis hüvasti.", text_w_blanks)

        text_w_blanks = Generator().createBlanks(text=text, partOfSpeech_list=["S"])
        self.assertEqual("Ta läks ära ja tuli hiljem tagasi. (mari) *Mari* polnud "
                         "kindel, mida ta tegema peaks ja jättis hüvasti.", text_w_blanks)

        text_w_blanks = Generator().createBlanks(text=text, partOfSpeech_list=["S"], verb_form_list=["Gen"])
        self.assertEqual("Ta läks ära ja tuli hiljem tagasi. (mari) *Mari* polnud "
                         "kindel, mida ta tegema peaks ja jättis hüvasti.", text_w_blanks)

        text_w_blanks = Generator().createBlanks(text=rista_tekst, partOfSpeech_list=['V'])
        self.assertEqual(
            '''Ristal (alustama) *alustab* sellest, kuidas ta Wolti juhi kohale üldse (jõudma) *jõudis*. 400 teist kandidaati, kaheksa pikka vestlust, kolm keerulist kodutööd, 120-slaidine tutvustus selle kohta, kuidas ta firma üles (ehtima) *ehitaks*. «Kui ma (olema) *olin* kogu selle kadalipu (läbima) *läbinud*, (ütlema) *ütlesin* omanikele, et (lõpetama) *lõpetame* selle jama ära, (andma) *andke* mulle leping, ma ju juba (töötama) *töötan* teie jaoks,» (meenutama) *meenutab* Ristal (naerma) *naerdes*. Tema motivatsioon (olema) *oli* kõrge, sest tal (olema) *oli* kindel plaan, missugune (pidama) *peaks* moodne toidu kojuveo firma (olema) *olema*.''',
            text_w_blanks)

        text_w_blanks = Generator().createBlanks(text=rista_tekst, partOfSpeech_list=['V'], age="noor",
                                                 lang_level=["A2"])
        self.assertEqual(
            '''Ristal alustab sellest, kuidas ta Wolti juhi kohale üldse jõudis. 400 teist kandidaati, kaheksa pikka vestlust, kolm keerulist kodutööd, 120-slaidine tutvustus selle kohta, kuidas ta firma üles ehitaks. «Kui ma olin kogu selle kadalipu läbinud, ütlesin omanikele, et lõpetame selle jama ära, andke mulle leping, ma ju juba töötan teie jaoks,» (meenutama) *meenutab* Ristal naerdes. Tema motivatsioon oli kõrge, sest tal oli kindel plaan, missugune peaks moodne toidu kojuveo firma olema.''',
            text_w_blanks)

        text_w_blanks = Generator().createBlanks(text=rista_tekst, partOfSpeech_list=['V'], age="taiskasvanu",
                                                 lang_level=["A2"])
        self.assertEqual(
            '''Ristal alustab sellest, kuidas ta Wolti juhi kohale üldse (jõudma) *jõudis*. 400 teist kandidaati, kaheksa pikka vestlust, kolm keerulist kodutööd, 120-slaidine tutvustus selle kohta, kuidas ta firma üles ehitaks. «Kui ma olin kogu selle kadalipu läbinud, ütlesin omanikele, et lõpetame selle jama ära, andke mulle leping, ma ju juba töötan teie jaoks,» meenutab Ristal naerdes. Tema motivatsioon oli kõrge, sest tal oli kindel plaan, missugune peaks moodne toidu kojuveo firma olema.''',
            text_w_blanks)

        text_w_blanks = Generator().createBlanks(text=rista_tekst, partOfSpeech_list=['V'], age="noor")
        self.assertEqual(
            '''Ristal (alustama) *alustab* sellest, kuidas ta Wolti juhi kohale üldse (jõudma) *jõudis*. 400 teist kandidaati, kaheksa pikka vestlust, kolm keerulist kodutööd, 120-slaidine tutvustus selle kohta, kuidas ta firma üles (ehtima) *ehitaks*. «Kui ma (olema) *olin* kogu selle kadalipu (läbima) *läbinud*, (ütlema) *ütlesin* omanikele, et (lõpetama) *lõpetame* selle jama ära, (andma) *andke* mulle leping, ma ju juba (töötama) *töötan* teie jaoks,» (meenutama) *meenutab* Ristal (naerma) *naerdes*. Tema motivatsioon (olema) *oli* kõrge, sest tal (olema) *oli* kindel plaan, missugune (pidama) *peaks* moodne toidu kojuveo firma (olema) *olema*.''',
            text_w_blanks)

        text_w_blanks = Generator().createBlanks(text=rista_tekst, partOfSpeech_list=['V'], age="taiskasvanu")
        self.assertEqual(
            '''Ristal (alustama) *alustab* sellest, kuidas ta Wolti juhi kohale üldse (jõudma) *jõudis*. 400 teist kandidaati, kaheksa pikka vestlust, kolm keerulist kodutööd, 120-slaidine tutvustus selle kohta, kuidas ta firma üles (ehtima) *ehitaks*. «Kui ma (olema) *olin* kogu selle kadalipu (läbima) *läbinud*, (ütlema) *ütlesin* omanikele, et (lõpetama) *lõpetame* selle jama ära, (andma) *andke* mulle leping, ma ju juba (töötama) *töötan* teie jaoks,» (meenutama) *meenutab* Ristal (naerma) *naerdes*. Tema motivatsioon (olema) *oli* kõrge, sest tal (olema) *oli* kindel plaan, missugune (pidama) *peaks* moodne toidu kojuveo firma (olema) *olema*.''',
            text_w_blanks)

        text_w_blanks = Generator().createBlanks(text=rista_tekst, partOfSpeech_list=['V'], age="noor")
        self.assertEqual((
            '''Ristal (alustama) *alustab* sellest, kuidas ta Wolti juhi kohale üldse (jõudma) *jõudis*. 400 teist kandidaati, kaheksa pikka vestlust, kolm keerulist kodutööd, 120-slaidine tutvustus selle kohta, kuidas ta firma üles (ehtima) *ehitaks*. «Kui ma (olema) *olin* kogu selle kadalipu (läbima) *läbinud*, (ütlema) *ütlesin* omanikele, et (lõpetama) *lõpetame* selle jama ära, (andma) *andke* mulle leping, ma ju juba (töötama) *töötan* teie jaoks,» (meenutama) *meenutab* Ristal (naerma) *naerdes*. Tema motivatsioon (olema) *oli* kõrge, sest tal (olema) *oli* kindel plaan, missugune (pidama) *peaks* moodne toidu kojuveo firma (olema) *olema*.'''),
            text_w_blanks)

    def test_inputs(self):

        self.assertFalse(Generator().check_inputs(["app" + SEP + "generator.py", "test.txt"]))
        self.assertFalse(Generator().check_inputs(
            ["app" + SEP + "generator.py", "test.txt", "V", "Sg", "False", "Some other argument"]))
        self.assertFalse(
            Generator().check_inputs(["app" + SEP + "generator.py", "nonExistentTextFile.txt", "V", "Sg", "False"]))
        self.assertFalse(Generator().check_inputs(["app" + SEP + "generator.py", "test.txt", "v", "None", "False"]))
        self.assertFalse(Generator().check_inputs(["app" + SEP + "generator.py", "test.txt", "V", "ggg", "False"]))
        self.assertFalse(Generator().check_inputs(["app" + SEP + "generator.py", "test.txt", "A", "Prs", "False"]))
        self.assertFalse(Generator().check_inputs(["app" + SEP + "generator.py", "test.txt", "A", "Gen", "False"]))

        POStag_list = ["A", "C", "D", "G", "H", "I", "J", "K", "N", "O", "P", "S", "U", "V", "X", "Y", "Z"];
        for POStag in POStag_list:
            self.assertTrue(Generator().check_inputs(
                ["app" + SEP + "generator.py", "test.txt", POStag, "None", "None", "None", "taiskasvanu", "None", "False", "None", "Pealkiri on siin", "True"]))

        noun_form_list = ["Sg", "Pl", "Nom", "Gen", "Par", "Ill", "Ine", "Ela", "All", "Ade", "Abl", "Tra", "Trm",
                          "Ess", "Abe", "Com"]
        for noun_form in noun_form_list:
            self.assertTrue(Generator().check_inputs(
                ["app" + SEP + "generator.py", "test.txt", "S", noun_form, "None", "None", "taiskasvanu", "None", "False", "None", "Pealkiri on siin", "True"]))

        verb_form_list = ["Impers", "Pers", "Prs", "Prt", "Ind", "Cond", "Imprt", "Quot", "Sg", "Pl", "1", "2", "3",
                          "Aff", "Neg", "Sup", "Inf", "Ger", "Prc"]
        for verb_form in verb_form_list:
            self.assertTrue(Generator().check_inputs(
                ["app" + SEP + "generator.py", "test.txt", "V", "None", verb_form, "None", "taiskasvanu", "None", "False", "None", "Pealkiri on siin", "True"]))

        pronoun_type_list = ["ps1","ps2","ps3","rec","refl","pos","dem","inter_rel","det","indef"]
        for pronoun_type in pronoun_type_list:
            self.assertTrue(Generator().check_inputs(
                ["app" + SEP + "generator.py", "test.txt", "V", "None", "None", pronoun_type, "taiskasvanu", "None",
                 "False", "None", "Pealkiri on siin", "True"]))

        difficulties_list = ["eelA1", "A1", "A2", "B1", "B2", "C1"]
        for difficulty in difficulties_list:
            self.assertTrue(Generator().check_inputs(
                ["app" + SEP + "generator.py", "test.txt", "V", "None", "None", "None", "taiskasvanu", difficulty,
                 "False", "None", "Pealkiri on siin", "True"]))

    def test_h5p_file(self):
        testFileName = "testingFile.h5p"
        text = "Ta (minema) *läks* ära ja (tulema) *tuli* hiljem tagasi. Mari (olema) *polnud* kindel, " \
               "mida ta (tegema) *tegema* (pidama) *peaks* ja (jätma) *jättis* hüvasti."

        if os.path.isfile("h5ptests" + SEP + testFileName):
            os.remove("h5ptests" + SEP + testFileName)

        if os.path.isdir("h5ptests" + SEP + "tempFolder"):
            shutil.rmtree("h5ptests" + SEP + "tempFolder")

        Generator().createH5Pfile(text, testFileName, "Hea pealkiri")
        self.assertTrue(os.path.isfile("h5ptests" + SEP + testFileName))

        zip = zipfile.ZipFile("h5ptests" + SEP + testFileName, 'r')
        zip.extractall("h5ptests" + SEP + "tempFolder")
        zip.close()

        file = open("h5ptests" + SEP + "tempFolder" + SEP + "content" + SEP + "content.json", "r", encoding='utf-8')
        data = file.read()
        d = json.loads(data)
        file.close()

        shutil.rmtree("h5ptests" + SEP + "tempFolder")
        os.remove("h5ptests" + SEP + testFileName)

        self.assertEqual(d["questions"][0], "<p>" + text + "</p>\n")

    def tearDown(self):
        if 'test_tagger.py' in os.listdir():
            os.chdir('..')
