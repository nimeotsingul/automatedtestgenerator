import unittest
import os
import sys

from estnltk.taggers import GTMorphConverter

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from cliapp.generator import Generator
from estnltk import Text

SEP = os.path.sep


class MyTestCase(unittest.TestCase):
    def setUp(self):
        if "automatedtestgenerator" in os.listdir():
            os.chdir('automatedtestgenerator' + SEP + 'tests')

    def test_blanks_noun_forms(self):
        text = "Nad tulid majast välja ja kõndisid metsa poole. Neile tuli vastu auto. Autost tuli välja Toomas. Nad läksid autosse ja Toomas jäi maha. Nad istusid autos ja sõitsid linnast välja. Nad panid lilled vaasi ja tee tassi. Liisa valas vett tassi. Lase vanni vett juurde."
        generator = Generator()
        tekst = Text(text)
        tekst.tag_layer(['words', 'sentences', 'morph_analysis', 'clauses'])
        gt_converter = GTMorphConverter()
        gt_converter.tag(tekst)
        text_w_blanks = generator.createBlanks(text, partOfSpeech_list=["S"], noun_form_list=["Ill", "Ine", "Ela"])
        self.assertEqual(
            "Nad tulid (maja) *majast* välja ja kõndisid metsa poole. Neile tuli vastu auto. Autost tuli välja Toomas. Nad läksid (auto) *autosse* ja Toomas jäi maha. Nad istusid (auto) *autos* ja sõitsid (linn) *linnast* välja. Nad panid lilled vaasi ja tee (tass) *tassi*. Liisa valas vett (tass) *tassi*. Lase (vann) *vanni* vett juurde.",
            text_w_blanks)

    def test_blanks_no_forms(self):
        text = "Ta läks ära ja tuli hiljem tagasi. Mari polnud kindel, mida ta tegema peaks ja jättis hüvasti."
        generator = Generator()
        text_w_blanks = generator.createBlanks(text, partOfSpeech_list=["V"])
        self.assertEqual(
            "Ta (minema) *läks* ära ja (tulema) *tuli* hiljem tagasi. Mari (olema) *polnud* kindel, mida ta"
            " (tegema) *tegema* (pidama) *peaks* ja (jätma) *jättis* hüvasti.", text_w_blanks)

    def test_h5p_creation(self):
        arr = os.listdir()
        if 'automatedtestgenerator' in arr:
            print(os.listdir('automatedtestgenerator'))
            os.chdir(SEP + 'automatedtestgenerator')
        text = "Ta oli siin."
        generator = Generator()
        generator.createH5Pfile(text, "unittest.h5p", title="Pealkiri")
        arr = os.listdir("." + SEP + "h5ptests")
        self.assertIn('unittest.h5p', arr)
        # testfali kustutamine
        os.remove("." + SEP + "h5ptests" + SEP + "unittest.h5p")

    def tearDown(self):
        if 'test_tagger.py' in os.listdir():
            os.chdir('..')


if __name__ == '__main__':
    unittest.main()
