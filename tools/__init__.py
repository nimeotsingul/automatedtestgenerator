from flask import Flask
from os import path

myapp = Flask(__name__, template_folder='../templates', static_folder='../static')
if path.exists('flask_config.py'):
    from flask_config import AppConfig
    myapp.config.from_object(AppConfig)


import app