import os
import csv
from estnltk import Text
from estnltk.taggers import GTMorphConverter

SEP = os.path.sep


# Analüüsimiseks class, mis ilma teste tegemata annab tagide ja vormide ülevaate
class Analyzer:
    # Kontrollib kas CLI input on õige
    def checkInputs(self, arguments):

        if not len(arguments) == 4:
            print("Vale number parameetreid")
            return False

        files = arguments[2]
        multiple = arguments[3]

        if not all(os.path.isfile(file) for file in files.split(";")):
            print("Teksti faili/faile ei leitud.")
            return False
        if multiple not in ['False', 'True']:
            print("Mitme testi argument peab olema True või False.")
            return False
        return True

    # Leiab tagid ja vormid teksti sees
    def findTextDetails(self, text):
        foundTextParts = {}

        text = Text(text)
        text.tag_layer(['words', 'sentences', 'morph_analysis', 'clauses'])

        words = text['words'].text
        if len(words) < 6:
            return None
        tags = text['words'].partofspeech

        # Valmistab ette GTMorphConverter'i
        gt_converter = GTMorphConverter()
        gt_converter.tag(text)
        forms = text['gt_morph_analysis'].form

        noun_form_list = ["Sg", "Pl", "Nom", "Gen", "Par", "Ill", "Ine", "Ela", "All", "Ade", "Abl", "Tra", "Trm",
                          "Ess", "Abe", "Com"]
        verb_form_list = ["Impers", "Pers", "Prs", "Prt", "Ind", "Cond", "Imprt", "Quot", "Sg", "Pl", "1", "2", "3",
                          "Aff", "Ill",
                          "Neg", "Sup", "Inf", "Ger", "Prc"]

        # Loetleb üle kõik teksti sõnad
        for i in range(len(tags)):
            tag = tags[i][0]

            # Salvestab leitud POStagi
            if tag in foundTextParts.keys():
                foundTextParts[tag] = foundTextParts[tag] + 1
            else:
                foundTextParts[tag] = 1

            # Kui nimisõna või tegusõna, salvestab ka selle vormi
            if tag == 'S' or tag == 'V':
                # Vaatab iga vormi sel sõnal
                for form in forms[i][0].split(' '):
                    saveForm = False
                    dict_form = ""
                    if tag == "S" and form in noun_form_list:
                        if form == "Sg" or form == "Pl" or form == "Ill":
                            dict_form = "S_" + form
                        else:
                            dict_form = form
                        saveForm = True
                    elif tag == "V" and form in verb_form_list:
                        if form == "Sg" or form == "Pl" or form == "Ill":
                            dict_form = "V_" + form
                        else:
                            dict_form = form
                        saveForm = True

                    if saveForm:
                        if dict_form in foundTextParts.keys():
                            foundTextParts[dict_form] = foundTextParts[dict_form] + 1
                        else:
                            foundTextParts[dict_form] = 1

        return foundTextParts

    # Peamine funktsioon, võtab sisendina failid, kust iga teksti üle vaatab
    def analyzeCSV(self, argumentList):
        if self.checkInputs(argumentList):
            fileNames = argumentList[2].split(';')
            multipleTests = argumentList[3] == 'True'

            columnNames = ["Testindeks", "Fail",
                           "A", "C", "D", "G", "H", "I", "J", "K", "N", "O", "P", "S", "U", "V", "X", "Y", "Z",
                           "S_Sg", "S_Pl", "Nom", "Gen", "Par", "S_Ill", "Ine", "Ela", "All", "Ade", "Abl", "Tra",
                           "Trm",
                           "Ess", "Abe", "Com",
                           "Impers", "Pers", "Prs", "Prt", "Ind", "Cond", "Imprt", "Quot", "V_Sg", "V_Pl", "V_Ill",
                           "1", "2", "3", "Aff", "Neg",
                           "Sup", "Inf", "Ger", "Prc"]

            # Loob uue csv faili
            csvFile = open("h5ptests" + SEP + "analyys.csv", mode='w', newline='')
            csvWriter = csv.writer(csvFile)
            csvWriter.writerow(columnNames)

            testiIndeks = 0
            # Loetleb üle failid
            for i in range(len(fileNames)):
                # Loeb faili sisse
                file = open(fileNames[i], "r", encoding='utf-8', errors='ignore')
                inputText = file.read()
                file.close()

                # Vaatab kas peab failist mitu erinevat testi tegema
                texts = []
                if multipleTests:
                    for text in inputText.split('\n\n<ERALDA>\n\n'):
                        texts.append(text)
                else:
                    inputText = inputText.replace('\n\n<ERALDA>\n\n', '\n\n')
                    texts.append(inputText)

                # Vaatab iga teksti läbi ja leiab nende sõnade detailid
                for j in range(len(texts)):

                    textDetails = self.findTextDetails(text=texts[j])

                    if not textDetails:
                        print('''Tekst "''' + texts[j] + '''" jäeti pikkuse tõttu tekstiks tegemata''')
                        continue

                    nextCSVline = [0] * len(columnNames)
                    nextCSVline[columnNames.index("Testindeks")] = testiIndeks
                    testiIndeks = testiIndeks + 1
                    nextCSVline[columnNames.index("Fail")] = fileNames[i]

                    for key in textDetails.keys():  # Tagid ja vormid
                        nextCSVline[columnNames.index(key)] = textDetails[key]

                    csvWriter.writerow(nextCSVline)

            csvFile.close()
            print("Analüüsi faili saab leida: " + os.getcwd() + SEP + "h5ptests" + SEP)
