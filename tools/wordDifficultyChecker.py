import os
import json
from resources import resourcesFile
import inspect
import pathlib


# Loeb faili json'ina sisse
def readInFileAsDict(fileName):
    filePath = os.path.join(os.path.dirname(inspect.getfile(resourcesFile)), fileName)

    with open(filePath, encoding='utf8') as json_file:
        data = json.load(json_file)
    return data


# Saab tõlgitud failinimed config failist
def getTranslatedFilename():
    filePath = os.path.join(os.path.dirname(pathlib.Path(__file__).parent.absolute()), "tools",
                            "wordDifficulty_config.json")
    with open(filePath, encoding='utf8') as json_file:
        data = json.load(json_file)
    return data


# Võtab estnltk POStagid ja tõlgib need sõnaliikideks, mida sonaVeeb kasutab
def checkPOSTagEstnltkToSonaveeb(wordPOSTag, sonaVeebPOSTag):
    translations = {
        'A': 'omadussõna',
        'C': 'omadussõna',
        'D': 'määrsõna',
        'H': 'nimisõna',
        'I': 'hüüdsõna',
        'J': 'sidesõna',
        'K': 'kaassõna',
        'N': 'arvsõna',
        'O': 'arvsõna',
        'P': 'asesõna',
        'S': 'nimisõna',
        'U': 'omadussõna',
        'V': 'tegusõna'
    }
    return translations[wordPOSTag] == sonaVeebPOSTag


class WordDifficultyChecker:

    def __init__(self):
        try:
            filenames = getTranslatedFilename()
            self.wordsDataYoung = readInFileAsDict(filenames.get('young_translated'))
            self.wordsDataAdult = readInFileAsDict(filenames.get('adult_translated'))
        except:
            print("Did not find translated sonaveeb files")

    # word - sõna lemma
    # wordPOSTag - sõna POStag
    # lang_level - Raskusastmed, mida sõna puhul vaadatakse
    # age - õppija vanuseaste (noor-keeleõppija or täiskasvanu)
    def findWordDifficulty(self, word, wordPOSTag, lang_level, age):
        difficulties = lang_level
        if difficulties == None:
            difficulties = ['eelA1', 'A1', 'A2', 'B1', 'B2', 'C1']
        if age is None:
            return False, None
        elif 'taiskasvanu' in age:
            wordsData = self.wordsDataAdult
        else:
            wordsData = self.wordsDataYoung

        # Vaatab kas sõna tähendus on salvestatud
        if word in wordsData.keys():
            # Tagastab listi sõna tähendustest
            wordDefinitions = wordsData.get(word)
            # Kui on ainult üks tähendus, siis vaata POStag'i ja annab selle
            if len(wordDefinitions) == 1:
                if checkPOSTagEstnltkToSonaveeb(wordPOSTag, wordDefinitions[0].get('POStag')):
                    wordDifficulty = wordDefinitions[0].get('difficulty')
                    return wordDifficulty in difficulties, wordDifficulty
                return False, None
            # Kui on rohkem tähendusi
            else:
                # Kontrollib, kas POS on sama iga tähenduse jaoks
                samePOS = []
                for definition in wordDefinitions:
                    if checkPOSTagEstnltkToSonaveeb(wordPOSTag, definition.get('POStag')):
                        samePOS.append(definition)

                # Kontrollib vasteid, kus on sama POS
                if len(samePOS) == 1:
                    wordDifficulty = samePOS[0].get('difficulty')
                    return wordDifficulty in difficulties, wordDifficulty
                # Kui on mitu vastet, siis vaatab raskusastmeid
                # Kontrollib neil on sama raskusaste, mis oli ette antud. Kui pole, siis vastus on None
                elif len(samePOS) > 1:
                    previousDif = ''
                    firstOne = True
                    for response in samePOS:
                        if firstOne:
                            firstOne = False
                            previousDif = response.get('difficulty')
                        else:
                            if response.get('difficulty') != previousDif:
                                return False, None
                    # Kui kõigil on sama raskusaste, siis tagastab need.
                    return previousDif in difficulties, previousDif
                return False, None
        else:
            return False, None
