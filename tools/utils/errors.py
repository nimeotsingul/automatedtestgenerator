from flask import jsonify
from werkzeug.http import HTTP_STATUS_CODES


def error_response(status_code, message=None):
    payload = {'error': HTTP_STATUS_CODES.get(status_code, 'Unknown error')}
    if message:
        payload['message'] = message
    response = jsonify(payload)
    response.status_code = status_code
    return response


def success_response(status_code, message=None):
    payload = {'success': HTTP_STATUS_CODES.get(status_code, 'SUCCESS')}
    if message:
        payload['message'] = message
        response = jsonify(payload)
        response.status_code = status_code
        return response


def bad_request(message):
    return error_response(400, message)


def notfound_request(message):
    return error_response(404, message)


def good_request(message):
    return success_response(200, message)

