import os
import json
from estnltk.wordnet import Wordnet
from estnltk import Text
from resources import resourcesFile
import inspect

# Seda faili kasutatakse EKI (Sõnaveeb) failide tõlkimiseks sobivale kujule.
# Sõnad tehakse WordNeti jaoks tuntavateks sümboliteks, et sünonüüme saaks eristada.
# See tähendab, et programm vajab vahest kasutaja abi.

"""
{
'word': word                - sõna
'POStag': POStag            - sõnaliik
'difficulty': difficulty    - sõna raskusaste [eelA1, A1, A2, B1, B2, C1]
'helper': helper            - selgitav sõna või ''
'synset': wordnet           - Juhul kui kasutati helperit. Muul juhul ''
}
"""

# Kui helper on üksik sõna, uuri kas leidub synset, mis see sõna tõenäoliselt on.
def checkSingleHelper(wn, word, helper):

    # Minimaalne sarnasuse protsent
    similarityPercentMinimum = 0.75

    word = Text(word)
    helper = Text(helper)
    word.tag_layer(['words', 'morph_analysis'])
    helper.tag_layer(['words', 'morph_analysis'])

    bestSynset = None
    bestPercent = -1

    if word['words'].lemma[0] == helper['words'].lemma[0]:
        # TODO lisada viis kuidas otsustada kuidas sõna kääne on seotud tavavormiga.
        pass
    else:
        # Sellel juhul on sõna erinev.
        # Uuri kas synset sarnasus on piisav.
        firstSynset = wn[word['words'].text[0]]
        secondSynset = wn[helper['words'].text[0]]
        for i in range(len(firstSynset)):
            for j in range(len(secondSynset)):
                percent = wn.path_similarity(firstSynset[i],secondSynset[j])
                if percent and percent > bestPercent:
                    bestPercent = percent
                    bestSynset = firstSynset[i]


    if bestPercent > similarityPercentMinimum:
        return bestSynset
    return None


def checkMultipleHelper(wn, word, helper):
    # Mitme helperiga
    # TODO kui on võimalik
    return None


def translateFile(fileName):
    # TODO parandus path'ide jaoks

    wn = Wordnet()

    # Avab ja loeb faili, mida muuta
    originalFile = open(os.path.join(os.path.dirname(inspect.getfile(resourcesFile)), fileName), encoding='utf-8')
    originalText = originalFile.readlines()

    # Sõnastik kõikide ridade jaoks, mida lisatakse
    wordDictionary = {}

    # First line of file is skipped, it is title
    # Esimene rida failis jäetakse vahele. See on pealkiri.
    firstLine = True
    for line in originalText:
        if firstLine:
            firstLine = False
            continue
        # Rida tehakse osadeks
        line.strip('\n')
        parts = line.split('\t')  # Rida tehakse osadeks ('word', 'type', 'difficulty'), mõndades ridades on veel midagi

        wordPlusHelper = parts[0]
        word = ''
        helper = ''
        POStag = parts[1]
        difficulty = parts[2]
        synset = ''

        # Kontrollib, kas sõnal on 'helper', mis on selgitav sõna sünonüümide puhuks. Esineb sulgudes.
        helperFound = False
        helperIndex = 0
        for i in range(len(wordPlusHelper)):
            symbol = wordPlusHelper[i]
            if symbol == '(':
                helperFound = True
                helperIndex = i
                break

        # Kas on helper või mitte
        if helperFound:
            word = wordPlusHelper.split(' ')[0]
            helper = wordPlusHelper[helperIndex+1:-1]
        else:
            word = wordPlusHelper

        # Kontrolli, kas sõna on juba nähtud. Kui pole, lisa tühi list.
        if word not in wordDictionary.keys():
            wordDictionary[word] = []

        # Kui sõnal polnud helperit, siis see tähendab, et sõnal on ainult üks tähendus
        if helper != '':
            # Uuritakse Wordnet synset vasteid

            # Kui helper on mitmesõnaline
            if len(helper.split(' ')) > 1:
                synset = checkMultipleHelper(wn, word,helper)
            else:
                # Helper on üks sõna. See tähendab, et see on sama sõna käänatud kujul või ühesõnaline selgitus
                synset = checkSingleHelper(wn, word, helper)
            if not synset:
                synset = ''

        wordDictionary[word].append(
            {'POStag': POStag,
             'difficulty': difficulty.replace('\n','')}
        )

    originalFile.close()

    # Loob uue faili vana nimega + _translated ja salvestab muudatused.
    newFileName = fileName.replace('.txt','') + '_translated.json'
    newFilePath = os.path.join(os.path.dirname(inspect.getfile(resourcesFile)), newFileName)

    # JSON dumps faili
    with open(newFilePath, 'w', encoding='utf8') as outfile:
        json.dump(wordDictionary, outfile, ensure_ascii=False)

    # Tagastab loodud faili nime.
    return newFileName

#translateFile('EKI_noor.txt')
#translateFile('EKI_etLex.txt')