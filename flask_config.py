import os


class AppConfig():
    UPLOAD_FOLDER = 'h5ptests'
    MAX_CONTENT_LENGTH = 16 * 1024 * 1024
    SECRET_KEY = 'JSOgrno54ODjj#fgjr_34)94gsd'
    JSON_AS_ASCII = False
