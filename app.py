from flask import request, send_from_directory, render_template, jsonify
from cerberus import Validator
from cliapp.generator import Generator
from tools.utils.errors import bad_request, notfound_request
import os
from tools import myapp

SEP = os.path.sep
ALLOWED_EXTENSIONS = {'txt'}

makeBlanksSchema = {
    'text': {'type': 'list', 'required': True},
    'postypes[]': {'type': 'list',
                   'allowed': ["A", "C", "D", "G", "H", "I", "J", "K", "N", "O", "P", "S", "U", "V", "X", "Y", "Z"],
                   'required': True},
    'nounforms[]': {'type': 'list',
                    'allowed': ["Sg", "Pl", "Nom", "Gen", "Par", "Ill", "Ine", "Ela", "All", "Ade", "Abl", "Tra", "Trm",
                                "Ess", "Abe", "Com"],
                    'required': False},
    'verbforms[]': {'type': 'list',
                    'allowed': ["Impers", "Pers", "Prs", "Prt", "Ind", "Cond", "Imprt", "Quot", "Sg", "Pl", "1", "2",
                                "3",
                                "Aff", "Neg", "Sup", "Inf", "Ger", "Prc"],
                    'required': False},
    'pronounforms[]': {'type': 'list',
                    'allowed': ["ps1","ps2","ps3","rec","refl","pos","dem","inter_rel","det","indef"],
                    'required': False},
    'age': {'type': 'list', 'allowed': ['young', 'adult'], 'required': True},
    'langlevel[]': {'type': 'list', 'allowed': ['eelA1', 'A1', 'A2', 'B1', 'B2', 'C1'], 'required': False},
    'title': {'type': 'list', 'required': False},
    'description': {'type': 'list', 'required': False},
}

makeH5PSchema = {
    'h_text': {'type': 'list', 'required': True},
    'h_title': {'type': 'list', 'required': True},
    'h_desc': {'type': 'list', 'required': False},
    'h_scoring': {'type': 'list', 'required': True},
    'hidden_submit': {'required': False}
}

downloadSchema = {
    'text': {'type': 'list', 'required': True},
    'overallFeedback[0][from]': {'type': 'list', 'required': False},
    'overallFeedback[0][to]': {'type': 'list', 'required': False},
    'overallFeedback[0][message]': {'type': 'list', 'required': False},
    'overallFeedback[1][from]': {'type': 'list', 'required': False},
    'overallFeedback[1][to]': {'type': 'list', 'required': False},
    'overallFeedback[1][message]': {'type': 'list', 'required': False},
    'overallFeedback[2][from]': {'type': 'list', 'required': False},
    'overallFeedback[2][to]': {'type': 'list', 'required': False},
    'overallFeedback[2][message]': {'type': 'list', 'required': False},
    'overallFeedback[3][from]': {'type': 'list', 'required': False},
    'overallFeedback[3][to]': {'type': 'list', 'required': False},
    'overallFeedback[3][message]': {'type': 'list', 'required': False},
    'overallFeedback[4][from]': {'type': 'list', 'required': False},
    'overallFeedback[4][to]': {'type': 'list', 'required': False},
    'overallFeedback[4][message]': {'type': 'list', 'required': False},
    'overallFeedback[5][from]': {'type': 'list', 'required': False},
    'overallFeedback[5][to]': {'type': 'list', 'required': False},
    'overallFeedback[5][message]': {'type': 'list', 'required': False},
    'overallFeedback[6][from]': {'type': 'list', 'required': False},
    'overallFeedback[6][to]': {'type': 'list', 'required': False},
    'overallFeedback[6][message]': {'type': 'list', 'required': False},
    'overallFeedback[7][from]': {'type': 'list', 'required': False},
    'overallFeedback[7][to]': {'type': 'list', 'required': False},
    'overallFeedback[7][message]': {'type': 'list', 'required': False},
    'overallFeedback[8][from]': {'type': 'list', 'required': False},
    'overallFeedback[8][to]': {'type': 'list', 'required': False},
    'overallFeedback[8][message]': {'type': 'list', 'required': False},
    'overallFeedback[9][from]': {'type': 'list', 'required': False},
    'overallFeedback[9][to]': {'type': 'list', 'required': False},
    'overallFeedback[9][message]': {'type': 'list', 'required': False},
    'overallFeedback[10][from]': {'type': 'list', 'required': False},
    'overallFeedback[10][to]': {'type': 'list', 'required': False},
    'overallFeedback[10][message]': {'type': 'list', 'required': False},
}


# Asendab Väljundi kastis oleva teksti generaatori väljundiga (lünkdaga tekst)
@myapp.route('/Create', methods=['POST'])
def create():
    if request.method == 'POST':
        formdata = request.form.to_dict(flat=False) or {}
        validator = Validator(makeBlanksSchema)
        if not validator.validate(formdata):
            # print(validator.errors)
            return bad_request(validator.errors)

        input_text = formdata.get('text')[0]
        POStags = formdata.get('postypes[]')
        noun_forms = formdata.get('nounforms[]', None)
        verb_forms = formdata.get('verbforms[]', None)
        pronoun_forms = formdata.get('pronounforms[]', None)
        age_level = formdata.get('age', None)
        if "young" in age_level:
            age_level = ["noor"]
        elif "adult" in age_level:
            age_level = ["taiskasvanu"]
        difficulties = formdata.get('langlevel[]', None)
        generaator = Generator()
        output_text = generaator.createBlanks(text=input_text,
                                              partOfSpeech_list=POStags,
                                              noun_form_list=noun_forms,
                                              verb_form_list=verb_forms,
                                              pronoun_type_list=pronoun_forms,
                                              age=age_level,
                                              lang_level=difficulties)
        formdata['text'] = output_text
        formdata.pop('postypes[]')
        formdata['tags'] = POStags

        return jsonify(formdata)


# Main page
@myapp.route('/', methods=['GET', 'POST'])
def index():
    if request.method == "POST":
        data = request.form.to_dict(flat=False) or {}
        validator = Validator(makeH5PSchema)
        if not validator.validate(data):
            # print(validator.errors)
            return bad_request(validator.errors)
        generaator = Generator()
        filename = "lynktest.h5p"
        overallFeedback = None
        feedbackList = data.get('h_scoring', [""])[0]
        if feedbackList != "":
            try:
                feedbackList = feedbackList.split(",|")
                overallFeedback = []
                for feedback in feedbackList:
                    if feedback == "0_comma_100_comma_":
                        overallFeedback = None
                        break
                    from_to_message = feedback.split("_comma_")
                    range = dict()
                    range['from'] = int(from_to_message[0])
                    range['to'] = int(from_to_message[1])
                    range['feedback'] = from_to_message[2]
                    overallFeedback.append(range)
            except:
                overallFeedback = None
        generaator.createH5Pfile(data.get('h_text')[0], filename, data.get('h_title')[0], data.get('h_desc',[""])[0], overallFeedback, True)
        try:
            return send_from_directory('..' + SEP + 'h5ptests', filename=filename, as_attachment=True)
        except FileNotFoundError:
            return notfound_request(FileNotFoundError.filename)
    return render_template("index.html")


@myapp.route('/Download', methods=['POST'])
def download_f():
    formdata = request.form.to_dict(flat=False) or {}
    validator = Validator(downloadSchema)
    if not validator.validate(formdata):
        # print(validator.errors)
        return bad_request(validator.errors)
    feedback = []
    vahemik = ""
    for key, value in formdata.items():
        if "overallFeedback" in key:
            vahemik += str(value)[2:-2]
            if "from" in str(key) or "to" in str(key):
                vahemik += "_comma_"
            else:
                vahemik
                feedback.append(vahemik)
                vahemik = "|"
    output = dict()
    output['text'] = formdata.get('text')[0]
    output['feedback'] = feedback

    return jsonify(output)
